from airflow.utils.edgemodifier import Label
from datetime import datetime, timedelta
from textwrap import dedent
from airflow.operators.bash import BashOperator
from airflow import DAG
from embulk_tasks import create_embulk_extraction_task

# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

with DAG(
    'NorthwindELT',
    default_args=default_args,
    description='A ELT dag for the Northwind ECommerceData',
    schedule_interval=timedelta(days=1),
    start_date=datetime(2022, 3, 21),
    catchup=False,
    tags=['example'],
) as dag:
    dag.doc_md = """
        ELT Diária do banco de dados de ecommerce Northwind,
        começando em 2022-02-07. 
    """

    tables = [
        'categories',
        'customer_customer_demo',
        'customer_demographics',
        'customers',
        'employee_territories',
        'employees',
        'order_details',
        'orders',
        'products',
        'region',
        'shippers',
        'suppliers',
        'territories',
        'us_states',
    ]

    for table in tables:
        create_embulk_config_task = create_embulk_extraction_task(table)

        run_embulk_task = BashOperator(
            task_id=f'run_embulk_{table}',
            bash_command=f'echo "embulk ran for " {table}',
        )

        create_embulk_config_task >> run_embulk_task


    run_embulk_task.doc_md = dedent(
        """\
    #### Task Documentation

    Essa task extrai os dados do banco de dados postgres, parte de baixo do step 1 da imagem:

    ![img](https://user-images.githubusercontent.com/49417424/105993225-e2aefb00-6084-11eb-96af-3ec3716b151a.png)

    """
    )