import pathlib
from airflow.operators.bash import BashOperator
from airflow.hooks.base import BaseHook

CURRENT_DIR = pathlib.Path(__file__).parent.resolve()
EMBULK_BIN = '~/.embulk/bin/embulk'

def create_embulk_extraction_task(table):
    connection = BaseHook.get_connection("northwind_connection")
    base_config_path = f'{CURRENT_DIR}/embulk_configs/base_embulk_config_frow_northwind.yaml.liquid'


    task = BashOperator(
        task_id= f'embulk_extract_{table}',
        bash_command=f'{EMBULK_BIN} run {base_config_path}',
        env={
            "TABLE": table,
            "PASSWORD": connection.password,
            "HOST": connection.host,
            "USER": connection.login,
            "DATABASE": connection.schema,
            # "TARGET_DIR": table_target_dir
        }
    )

    return task